package alphabet

import (
	"strings"
)

const (
	ProteinLetters         = "ACDEFGHIKLMNPQRSTVWY"
	ExtendedProteinLetters = "ACDEFGHIKLMNPQRSTVWYBXZJUO"
	AmbiguousDNALetters    = "GATCRYWSMKHBVDN"
	UnambiguousDNALetters  = "GATC"
	AmbiguousRNALetters    = "GAUCRYWSMKHBVDN"
	UnambiguousRNALetters  = "GAUC"
	ExtendedDNALetters     = "GATCBDSW"
)

type Alphabet struct {
	Size    int
	Letters []string
}

func NewSingleLetterAlphabet() *Alphabet {
	return &Alphabet{Size: 1}
}

func NewProteinSingleLetterAlhpabet() *Alphabet {
	a := &Alphabet{Size: 1, Letters: strings.Split(ProteinLetters, "")}
	return a
}

func NewExtendedProteinLetterAlphabet() *Alphabet {
	a := &Alphabet{Size: 1, Letters: strings.Split(ExtendedProteinLetters, "")}
	return a
}

func NewDNASingleLetterAlhpabet() *Alphabet {
	a := &Alphabet{Size: 1, Letters: strings.Split(UnambiguousDNALetters, "")}
	return a
}

func NewAmbiguousDNASingleLetterAlhpabet() *Alphabet {
	a := &Alphabet{Size: 1, Letters: strings.Split(AmbiguousDNALetters, "")}
	return a
}

func NewExtendedDNASingleLetterAlhpabet() *Alphabet {
	a := &Alphabet{Size: 1, Letters: strings.Split(ExtendedDNALetters, "")}
	return a
}
