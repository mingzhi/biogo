package align

import (
	"io"
	"os/exec"
)

// Command line wrapper for the multiple alignment program MUSCLE.
// http://www.drive5.com/muscle/
func Muscle(stdin io.Reader, stdout, stderr io.Writer, options ...string) (err error) {
	cmd := exec.Command("muscle", options...)
	cmd.Stdin = stdin
	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err = cmd.Run()

	return
}
