package bioutil

import (
	"testing"
)

func TestGetCodons(t *testing.T) {
	{
		aa := "msrvsphql--tz"
		na := "ATgaGTCGGGTTTCACCGCATCAACTCACAGCACGGCAAGTCAGTGCTCTGAAGGGCGGTTCTCTATGTGATGGAGGCGGCCTTTGGTTAGTGGCTCAAGGCGCTGCAAAATCTTGGTTTTTTAGGTTCACCAGTCCTATTTCCAACACCCGAAGAGAGATGGGGCTGGGATCCGTCCATGACCTGAGTTTGGCGGAGGTTCGTCGAAAAGCCACTGAAGCACGACACTTGGTG"

		codonTable := BuildinCodonTables()["11"]
		codons, _ := GetCondons(aa, na, codonTable)
		if codons == nil {
			t.Log("can not find the codons")
		}
		if len(codons) != 11 {
			t.Logf("wrong number of resulted codons: %d", len(codons))
		}
		t.Log(codons)
	}

	{
		aa := "msrvsphqltarqvsalkggslcdggglwlvaqgaakswffrftspisn"
		na := "ATgAGTCGGGATTCACCGCATCAACTCACAGCACGGCAAGTCAGTGCTCTGAAGGGCGGTTCTCTATGTGATGGAGGCTGCCTTTGGTTAGTGGCTCAAGGCGCTGCAAAATCTTGGTTTTTTAGGTTCACCAGTCCTATTTCCAACACCCGAAGAGAGATGGGGCTGGGATCCGTCCATGACCTGAGTTTGGCGGAGGTTCGTCGAAAAGCCACTGAAGCACGACACTTGGTG"

		codonTable := BuildinCodonTables()["11"]
		codons, err := GetCondons(aa, na, codonTable)
		if codons == nil {
			t.Log("can not find the codons")
		}

		if err == nil {
			t.Error("there should be err returned.")
		}
		t.Log(err)
	}
}
