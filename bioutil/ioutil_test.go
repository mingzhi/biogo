package bioutil

import (
	"strings"
	"testing"
)

func TestPttReader(t *testing.T) {
	pttConent := `Acetobacter pasteurianus IFO 3283-01, complete genome - 1..2907495
2628 proteins
Location	Strand	Length	PID	Gene	Synonym	Code	COG	Product
388..813	-	141	258541106	rusA	APA01_00010	-	COG4570L	Holliday junction resolvase RusA
1206..2420	-	404	258541107	-	APA01_00020	-	COG0582L	phage integrase
2681..3160	-	159	258541108	-	APA01_00030	-	COG1671S	hypothetical protein
3400..3927	+	175	258541109	-	APA01_00040	-	COG3678UNTP	hypothetical protein
4064..5311	+	415	258541110	bcr/cflA	APA01_00050	-	COG2814G	multidrug ABC transporter
5336..5722	-	128	258541111	-	APA01_00060	-	-	hypothetical protein
5753..5938	-	61	258541112	-	APA01_00070	-	COG2835S	hypothetical protein
5965..6669	-	234	258541113	-	APA01_00080	-	COG2802R	Lon-like ATP-dependent protease La
6684..7643	-	319	258541114	-	APA01_00090	-	COG3118O	thioredoxin
7706..7819	-	37	258541115	-	APA01_00100	-	-	hypothetical protein
7882..10746	+	954	258541116	uvrA	APA01_00110	-	COG0178L	excinuclease ABC subunit A
10746..11585	+	279	258541117	-	APA01_00120	-	COG1402R	creatininase
11693..12991	+	432	258541118	-	APA01_00130	-	COG3391S	hypothetical protein
`
	pttReader := NewPttReader(strings.NewReader(pttConent))

	records, err := pttReader.ReadAll()
	if err != nil {
		t.Error(err)
	}

	expectNumber := 13
	if len(records) != expectNumber {
		t.Errorf("expected %d, but got %d\n", expectNumber, len(records))
	}

	location := records[1].Location
	if location.Start != 1206 {
		t.Errorf("expected get start %d, but got %d\n", 1206, location.Start)
	}

	if location.End != 2420 {
		t.Errorf("expected getting end %d, but got %d\n", 2420, location.End)
	}

	if location.Strand != -1 {
		t.Errorf("expected getting strand -1, but got %d\n", location.Strand)
	}

	if records[1].Id != "258541107" {
		t.Errorf("expected getting id 258541107, but got %s\n", records[1].Id)
	}
}
