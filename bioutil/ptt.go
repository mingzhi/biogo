package bioutil

import (
	"bitbucket.org/mingzhi/biogo"
	"bufio"
	"io"
	"regexp"
	"strconv"
	"strings"
)

// A PttReader read records from a .ptt file from the NCBI ftp genome folder.
type PttReader struct {
	r *bufio.Reader
}

func NewPttReader(r io.Reader) *PttReader {
	return &PttReader{r: bufio.NewReader(r)}
}

func (r *PttReader) Read() (record *biogo.SeqFeature, err error) {
	record, err = r.parseRecord()
	return
}

func (r *PttReader) ReadAll() (records []*biogo.SeqFeature, err error) {
	for {
		record, err := r.Read()
		if err == io.EOF {
			return records, nil
		}
		if err != nil {
			return nil, err
		}
		records = append(records, record)

	}
	panic("unreachable")
}

func (r *PttReader) parseRecord() (record *biogo.SeqFeature, err error) {
	locationPattern, err := regexp.Compile(`^\d+\.\.\d+`)
	if err != nil {
		return
	}
	// skips any text before the first record.
	line, err := r.r.ReadString('\n')
	for err == nil {
		if locationPattern.MatchString(line) {
			break
		}
		line, err = r.r.ReadString('\n')
	}
	if err != nil {
		return
	}

	line = strings.TrimSpace(line)
	fields := strings.Split(line, "\t")
	location, err := parseLocation(fields[0])
	if err != nil {
		return nil, err
	}
	location.Strand = parseStrand(fields[1])
	feature := biogo.NewSeqFeature(fields[3])
	feature.Location = location

	record = feature
	return
}

func parseLocation(s string) (location *biogo.FeatureLocation, err error) {
	fields := strings.Split(s, "..")
	start, err := strconv.Atoi(fields[0])
	if err != nil {
		return
	}
	end, err := strconv.Atoi(fields[1])
	if err != nil {
		return
	}

	return biogo.NewFeatureLocation(start, end, 0), err
}

func parseStrand(s string) (strand int) {
	if s == "+" {
		strand = 1
	} else if s == "-" {
		strand = -1
	}

	return
}
