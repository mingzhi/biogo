package bioutil

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func PAL2NAL(aa, na string, codonTable CodonTable) (nal string, err error) {
	codons, err := GetCodons(aa, na, codonTable)
	if codons == nil {
		return
	} else {
		na = strings.Join(codons, "")
	}
	return
}

func getCodonPatterns(aa, na string, codonTable CodonTable) (cPatterns []string) {
	backTable := codonTable.BackTable
	for _, a := range aa {
		codon, found := backTable[string(a)]
		if found {
			if len(cPatterns) == 0 && a == 'M' { // start codon
				cPatterns = append(cPatterns, "("+backTable[">"]+")")
			} else {
				cPatterns = append(cPatterns, "("+codon+")")
			}
		} else if a == '-' || a == '.' { // gap, ignore
		} else if a == '*' { // stop codon
			cPatterns = append(cPatterns, "("+backTable["*"]+")")
		} else if matched, err := regexp.MatchString(`\d`, string(a)); matched && err == nil {
			num, _ := strconv.Atoi(string(a))
			s := make([]byte, num)
			for i := 0; i < num; i++ {
				s[i] = '-'
			}
			cPatterns = append(cPatterns, "("+string(s)+")")
		} else {
			cPatterns = append(cPatterns, "(...)")
		}
	}
	return
}

func GetCodons(aa, na string, codonTable CodonTable) (codons []string, err error) {
	aa = strings.ToUpper(aa)

	cPatterns := getCodonPatterns(aa, na, codonTable)

	codonReg, err := regexp.Compile(`(?i)` + strings.Join(cPatterns, ""))
	if err != nil {
		panic(err)
	}

	if codonReg.MatchString(na) {
		codons = codonReg.FindStringSubmatch(na)[1:]
		err = nil
		return
	}

	fragments := []string{}
	fraglen := 10
	fragnum := len(aa) / fraglen
	if len(aa)%fraglen == 0 {
		for i := 0; i < fragnum; i++ {
			fragments = append(fragments, aa[i*fraglen:(i+1)*fraglen])
		}
	} else {
		for i := 0; i < fragnum-1; i++ {
			fragments = append(fragments, aa[i*fraglen:(i+1)*fraglen])
		}
		fragments = append(fragments, aa[(fragnum-1)*fraglen:])
	}

	messages := []string{}

	cPatterns = []string{}
	for i, frag := range fragments {
		qcondons := getCodonPatterns(frag, na, codonTable)

		if matched, _ := regexp.MatchString(strings.Join(qcondons, ""), na); matched {
			cPatterns = append(cPatterns, strings.Join(qcondons, ""))
		} else {
			fcondons := make([]string, len(qcondons))
			for i := 0; i < len(qcondons); i++ {
				fcondons[i] = "(...)"
			}
			cPatterns = append(cPatterns, strings.Join(fcondons, ""))
			m := fmt.Sprintf("mismatch: start at %d, %s", i*fraglen, frag)
			messages = append(messages, m)
		}
	}

	codonReg, err = regexp.Compile("(?i)" + strings.Join(cPatterns, ""))
	if err != nil {
		panic(err)
	}

	if codonReg.MatchString(na) {
		codons = codonReg.FindStringSubmatch(na)[1:]
		err = errors.New(strings.Join(messages, "\n"))
		return
	}

	err = errors.New("can not get correct codons!")
	return
}
