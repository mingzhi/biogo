package seqio

import (
	"bitbucket.org/mingzhi/biogo"
	"bitbucket.org/mingzhi/biogo/alphabet"
	"bufio"
	"fmt"
	"io"
	"strings"
	"unicode"
)

// A ParseError is returned for parsing errors.
// The first line is 1.
type ParseError struct {
	Line int   // Line where the error occured
	Err  error // The actual error
}

func (e *ParseError) Error() string {
	return fmt.Sprintf("line %d: %s", e.Line, e.Err)
}

// A FastaReader reads records from a Fasta-encoded file.
type FastaReader struct {
	DeflineParser    func(string) string            // customized function to parse id from the defline.
	AnnotationParser func(string) map[string]string // customized function to get annotations.
	Alphabet         *alphabet.Alphabet             // alphabet for each sequence.
	line             int
	r                *bufio.Reader
}

// NewFastaReader returns a new Reader that reads from r.
func NewFastaReader(r io.Reader) *FastaReader {
	return &FastaReader{
		r: bufio.NewReader(r),
	}
}

// error creates a new ParseError based on err.
func (r *FastaReader) error(err error) error {
	return &ParseError{
		Line: r.line,
		Err:  err,
	}
}

// Read reads one record from r. The record is a SeqRecord.
func (r *FastaReader) Read() (record *biogo.SeqRecord, err error) {
	record, err = r.parseRecord()
	return
}

// unreadRune puts the last rune read from r back.
func (r *FastaReader) unreadRune() {
	r.r.UnreadRune()
}

// skip reads runes up to and including the rune delim or until error.
func (r *FastaReader) skip(delim rune) error {
	for {
		r1, _, err := r.r.ReadRune()
		if err != nil {
			return err
		}
		if r1 == delim {
			return nil
		}
	}
	panic("unreachable")
}

// parseRecord reads and parses a single biogo.SeqRecord from r.
func (r *FastaReader) parseRecord() (record *biogo.SeqRecord, err error) {
	title2id := r.DeflineParser
	if title2id == nil {
		title2id = func(title string) string { return strings.Split(title, " ")[0] }
	}
	// Skips any text before the first record.
	var r1 rune
	for err == nil {
		r1, _, err = r.r.ReadRune()
		if err != nil {
			return nil, err
		}
		if r1 == '>' {
			break
		} else {
			err = r.skip('\n')
		}
	}

	if err != nil {
		return nil, err
	}

	var defline string
	defline, err = r.r.ReadString('\n')
	if err != nil {
		return nil, err
	}

	var id, desc string
	var annotations map[string]string
	desc = strings.TrimRightFunc(defline, unicode.IsSpace)
	id = title2id(desc)
	if r.AnnotationParser != nil {
		annotations = r.AnnotationParser(desc)
	}

	// read sequences
	lines := []string{}

	for err == nil {
		r1, _, err = r.r.ReadRune()
		r.r.UnreadRune()
		if err != nil || r1 == '>' {
			break
		} else {
			var line string
			line, err = r.r.ReadString('\n')
			lines = append(lines, strings.TrimSpace(line))
		}
	}

	// create SeqRecord
	data := strings.Join(lines, "")
	seq := biogo.NewSequence(data, r.Alphabet)
	record = biogo.NewSeqRecord(id, desc, seq)
	record.Annotations = annotations

	return
}

// ReadAll reads all the remaining records from r.
// Each record is a *biogo.SeqRecord.
// A successful call return err == nil, not err == EOF.
// Because ReadAll is defined to read until EOF, it does not tread end of file as an error to be reported.
func (r *FastaReader) ReadAll() (records []*biogo.SeqRecord, err error) {
	for {
		record, err := r.Read()
		if err == io.EOF {
			records = append(records, record)
			return records, nil
		}
		if err != nil {
			return nil, err
		}
		records = append(records, record)
	}
	panic("unreachable")
}

// FastaIterator return a channel of *biogo.SeqRecord, which can act as a iterator.
func FastaIterator(source io.Reader, title2id func(string) string, alphabet *alphabet.Alphabet) (ch chan *biogo.SeqRecord) {
	if title2id == nil {
		title2id = func(title string) string { return strings.Split(title, " ")[0] }
	}

	ch = make(chan *biogo.SeqRecord)
	go func() {
		rd := bufio.NewReader(source)

		// Skips any text before the first records
		// (e.g. blank lines, comments)
		line, err := rd.ReadString('\n')
		for err == nil {
			if line[0] == '>' {
				break
			}
			line, err = rd.ReadString('\n')
		}

		if err != nil && err != io.EOF {
			panic(err)
		}

		for err == nil {
			// Handle the define line.
			desc := strings.TrimRightFunc(line, unicode.IsSpace)[1:]
			id := title2id(desc)

			// Get sequence data
			lines := []string{}
			line, err = rd.ReadString('\n')
			for err == nil {
				if line[0] == '>' {
					break
				}
				lines = append(lines, strings.TrimSpace(line))
				line, err = rd.ReadString('\n')
			}

			data := strings.Join(lines, "")
			seq := biogo.NewSequence(data, alphabet)
			record := biogo.NewSeqRecord(id, desc, seq)
			ch <- record

			if err != nil && err != io.EOF {
				panic(err)
			}
		}
		close(ch)
	}()

	return ch
}

// Class to write Fasta format files.
type FastaWriter struct {
	Wrap          int                           // line length used to wrap sequence lines.
	DeflineCreate func(*biogo.SeqRecord) string // function returns the defline of a *biogo.SeqRecord.
	w             *bufio.Writer
}

// Create a FastaWriter
func NewFastaWriter(w io.Writer) *FastaWriter {
	return &FastaWriter{
		w: bufio.NewWriter(w),
	}
}

// FastaWriter writes a single *biogo.SeqRecord to w.
func (w *FastaWriter) Write(record *biogo.SeqRecord) (err error) {
	// write defline.
	var defline string
	if w.DeflineCreate == nil {
		defline = ">" + record.Id + "\n"
	} else {
		defline = ">" + w.DeflineCreate(record) + "\n"
	}
	_, err = w.w.WriteString(defline)
	if err != nil {
		return
	}

	// write sequence.
	var nlines, llen int
	seq := record.Seq.Data
	if w.Wrap > 0 {
		nlines = len(seq) / w.Wrap
		llen = w.Wrap
	} else {
		nlines = 1
		llen = len(seq)
	}

	for i := 0; i < nlines; i++ {
		b := llen * i
		e := llen * (i + 1)
		_, err = w.w.WriteString(seq[b:e] + "\n")
		if err != nil {
			return
		}
	}

	if len(seq)%llen != 0 {
		_, err = w.w.WriteString(seq[llen*nlines:] + "\n")
	}

	return
}

// Flush writes any buffered data to the underlying io.Writer.
func (w *FastaWriter) Flush() {
	w.w.Flush()
}

// WriteAll writes multiple *biogo.SeqRecords to w using Write and then calls Flush.
func (w *FastaWriter) WriteAll(records []*biogo.SeqRecord) (err error) {
	for _, record := range records {
		err = w.Write(record)
		if err != nil {
			return
		}
	}

	w.Flush()

	return
}
