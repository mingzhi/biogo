package seqio

import (
	"bitbucket.org/mingzhi/biogo"
	"bitbucket.org/mingzhi/biogo/alphabet"
	"bytes"
	"fmt"
	"io"
	"strings"
	"testing"
)

var protein_fasta string

func init() {
	protein_fasta = `>gi|15718680|ref|NP_005537.3| tyrosine-protein kinase ITK/TSK [Homo sapiens]
MNNFILLEEQLIKKSQQKRRTSPSNFKVRFFVLTKASLAYFEDRHGKKRTLKGSIELSRIKCVEIVKSDI
SIPCHYKYPFQVVHDNYLLYVFAPDRESRQRWVLALKEETRNNNSLVPKYHPNFWMDGKWRCCSQLEKLA
TGCAQYDPTKNASKKPLPPTPEDNRRPLWEPEETVVIALYDYQTNDPQELALRRNEEYCLLDSSEIHWWR
VQDRNGHEGYVPSSYLVEKSPNNLETYEWYNKSISRDKAEKLLLDTGKEGAFMVRDSRTAGTYTVSVFTK
AVVSENNPCIKHYHIKETNDNPKRYYVAEKYVFDSIPLLINYHQHNGGGLVTRLRYPVCFGRQKAPVTAG
LRYGKWVIDPSELTFVQEIGSGQFGLVHLGYWLNKDKVAIKTIREGAMSEEDFIEEAEVMMKLSHPKLVQ
LYGVCLEQAPICLVFEFMEHGCLSDYLRTQRGLFAAETLLGMCLDVCEGMAYLEEACVIHRDLAARNCLV
GENQVIKVSDFGMTRFVLDDQYTSSTGTKFPVKWASPEVFSFSRYSSKSDVWSFGVLMWEVFSEGKIPYE
NRSNSEVVEDISTGFRLYKPRLASTHVYQIMNHCWKERPEDRPAFSRLLRQLAEIAESGL

>gi|157818821|ref|NP_001102295.1| tyrosine-protein kinase ITK/TSK [Rattus norvegicus]
MNNFILLEEQLIKKSQQKRRTSPSNFKVRFFVLTKASLAYFEDRHGKKRTLKGSIELSRIKCVEIVKSDI
SIPCHYKYPFQTLTSFQVMHDNYLLYVFAPDCESRQRWVLTLKEETRNNNSLVSKYHPNFWMDGRWRCCS
QLEKLAVGCAPYDPTKNASKKPLPPTPEDNRRSFQEPEETLVIALYDYQTNDPQELALRRDEEYYLLDSS
EIHWWRVQDKNGHEGYAPSSYLVEKSPNSLETYEWYNKSISRDKAEKLLLDTGKEGAFMVRDSRTPGTYT
VSVFTKAIISENNPCIKHYHIKETNDSPKRYYVAEKYVFDSIPLLIQYHQYNGGGLVTRLRYPVCSWRQK
APVTAGLRYGKWVIQPSELTFVQEIGSGQFGLVHLGYWLNKDKVAIKTIQEGAMSEEDFIEEAEVMMKLS
HPKLVQLYGVCLEQAPICLVFEFMEHGCLSDYLRSQRGLFAAETLLGMCLDVCEGMAYLEKACVIHRDLA
ARNCLVGENQVIKVSDFGMTRFVLDDQYTSSTGTKFPVKWASPEVFSFSRYSSKSDVWSFGVLMWEVFSE
GKIPYENRSNSEVVEDISTGFRLYKPRLASPHVYQVMNHCWKEKPEDRPPFSRLLSQLAEIAECGL

>gi|363738985|ref|XP_414568.3| PREDICTED: tyrosine-protein kinase ITK/TSK [Gallus gallus]
MSWERTILCPVMNNCVLLEEQLIKKSQQKRRTSPSNFKVRFFVLTKSKLAYYEHRHGKKRTLKGYVELSR
IKCVEIVKSDIIIPCQYKYPFQIVHDNYILYVFAPNRESRQRWVFTLKEETRSNNSLVSKYHPDFWIDGK
WRCCAQTEKMAVGCIEYDPTKTASKKPLPPTPEDNWKSLLNTKETLVMAIYDYEAQNPQELTLQYNEEYH
VIDSSEEHWWLIQDKNGHEGYVPSSYLAEKSPENLQIYEWYNKNINRSKAEMLLRDEGREGAFMVRDSRQ
PGMYTVSVFTKALSTDNNPVIKHYHINETTDFPKRYYLAEKHVFNCIPELINYHQHNAGGLVTRLRYAVS
SWKKKAPITAGLSYGKLVINPSELTRVQEIGSGQFGVVYLGYWLEKTKVAIKTIREGAMSEEDFIEEAKV
LMKLSHPKLVQLYGVCFENTPICLVFEFMEHGCLSDYLRSQRGSFSKETLLGMCLDVCEGMAYLEQNSVI
HRDLAARNCLVGESQVVKVSDFGMSRIVLDDQYTSSTGTKFPVKWSAPEVFSYSNYSTKSDVWSFGVLMW
EVFSEGKMPYENRTNGEVVEEINAGFRLYKPKLASKAIYEVMSHCWSMRKEDRPSFSLLLYQLSEISEFD
P`
}

func TestFastaRead(t *testing.T) {
	r := NewFastaReader(strings.NewReader(protein_fasta))
	r.DeflineParser = func(name string) string { return strings.Split(name, "|")[1] }
	s, err := r.Read()
	if err != nil {
		t.Error(err)
	}
	if s.Id != "15718680" {
		t.Errorf("Expected Id: %s, but Got: %s\n", "15718680", s.Id)
	}
	if len(s.Seq.Data) != 620 {
		t.Errorf("Expect to get a sequence of %daa, but got %d\n", 620, len(s.Seq.Data))
	}

	s, err = r.Read()
	if err != nil {
		t.Error(err)
	}
	if s.Id != "157818821" {
		t.Errorf("Expected Id: %s, but Got: %s\n", "15718680", s.Id)
	}
	if len(s.Seq.Data) != 626 {
		t.Errorf("Expect to get a sequence of %daa, but got %d\n", 626, len(s.Seq.Data))
	}

	s, err = r.Read()
	if err != io.EOF {
		t.Errorf("Expected EOF, but got %v\n", err)
	}
	if s.Id != "363738985" {
		t.Errorf("Expected Id: %s, but Got: %s\n", "15718680", s.Id)
	}
	if len(s.Seq.Data) != 631 {
		t.Errorf("Expect to get a sequence of %daa, but got %d\n", 631, len(s.Seq.Data))
	}

}

func TestFastaReadAll(t *testing.T) {
	r := NewFastaReader(strings.NewReader(protein_fasta))
	r.DeflineParser = func(name string) string { return strings.Split(name, "|")[1] }

	records, err := r.ReadAll()
	if err != nil {
		t.Error(err)
	}

	if len(records) != 3 {
		t.Errorf("Expect to get 3 sequences, but only got %d\n", len(records))
	}
}

func TestFastaIterator(t *testing.T) {
	giparser := func(name string) string { return strings.Split(name, "|")[1] }
	ch := FastaIterator(strings.NewReader(protein_fasta), giparser, alphabet.NewSingleLetterAlphabet())

	seqs := []*biogo.SeqRecord{}

	for seq := range ch {
		fmt.Println(seq.Id)
		seqs = append(seqs, seq)
	}

	if len(seqs) != 3 {
		t.Errorf("Expected 3 sequences, but got %d\n", len(seqs))
	}

	if len(seqs[1].Seq.Data) != 626 {
		t.Errorf("Expected length of second sequences: 626, but got %d\n", len(seqs[1].Seq.Data))
	}

	ch = FastaIterator(strings.NewReader(protein_fasta), nil, alphabet.NewSingleLetterAlphabet())
	for seq := range ch {
		fmt.Println(seq.Id)
	}
}

func TestFastaWrite(t *testing.T) {
	r := NewFastaReader(strings.NewReader(protein_fasta))
	r.DeflineParser = func(name string) string { return strings.Split(name, "|")[1] }
	records, err := r.ReadAll()
	if err != nil {
		t.Error(err)
	}

	record1 := records[0]
	out := new(bytes.Buffer)
	w := NewFastaWriter(out)
	w.Wrap = 60
	w.DeflineCreate = func(record *biogo.SeqRecord) string { return record.Id }
	err = w.Write(record1)
	if err != nil {
		t.Error(err)
	}
	w.Flush()
	content := string(out.Bytes()) // writed content.
	writed_lines := strings.Split(content, "\n")
	// check defline
	writed_defline := writed_lines[0]
	writed_defline = writed_defline[1:len(writed_defline)]
	if writed_defline != w.DeflineCreate(record1) {
		t.Errorf("Writed: %s, Original: %s\n", writed_defline, w.DeflineCreate(record1))
	}
	// check sequence
	for i := 1; i < len(writed_lines); i++ {
		var writed, orginl string
		writed = writed_lines[i]
		if (i-1)*w.Wrap >= len(record1.Seq.Data) {
			if len(writed) != 0 {
				t.Errorf("Should be end, but got %v\n", writed)
			}
		} else {
			if i*w.Wrap <= len(record1.Seq.Data) {
				orginl = record1.Seq.Data[(i-1)*w.Wrap : i*w.Wrap]
			} else {
				orginl = record1.Seq.Data[(i-1)*w.Wrap:]
			}
			if writed != orginl {
				t.Errorf("Writed: %s, Original: %s\n", writed, orginl)
			}
		}
	}

	fmt.Print(string(out.Bytes()))
}

func TestFastaWriteAll(t *testing.T) {
	r := NewFastaReader(strings.NewReader(protein_fasta))
	r.DeflineParser = func(name string) string { return strings.Split(name, "|")[1] }
	records, err := r.ReadAll()
	if err != nil {
		t.Error(err)
	}

	out := new(bytes.Buffer)
	w := NewFastaWriter(out)
	w.Wrap = 50
	w.DeflineCreate = func(record *biogo.SeqRecord) string { return record.Id }

	w.WriteAll(records)

	content := string(out.Bytes())
	writedLines := strings.Split(content, "\n")
	n := -1
	l := 0
	for i := 0; i < len(writedLines); i++ {
		wLine := writedLines[i]
		if len(wLine) > 0 {
			if wLine[0] == '>' {
				n++
				l = 0
				originalDefline := w.DeflineCreate(records[n])
				writedDefline := wLine[1:]
				if originalDefline != writedDefline {
					t.Errorf("Writed: %s, Original: %s\n", writedDefline, originalDefline)
				}
			} else {
				if l*w.Wrap >= len(records[n].Seq.Data) {
					t.Errorf("In seq %s, should end, but writed %s\n", records[n].Id, wLine)
				}
				var original string
				if (l+1)*w.Wrap <= len(records[n].Seq.Data) {
					original = records[n].Seq.Data[l*w.Wrap : (l+1)*w.Wrap]
				} else {
					original = records[n].Seq.Data[l*w.Wrap:]
				}
				if original != wLine {
					t.Errorf("Writed %s\n, Original: %s\n", wLine, original)
				}
				l++
			}
		}

	}
}
