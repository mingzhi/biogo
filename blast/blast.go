package blast

// Interacting with BLAST related applications.

import (
	"encoding/csv"
	"errors"
	"io"
	"os/exec"
	"strconv"
)

// NCBI Blast standard output entry.
type TableEntry struct {
	QSeqid   string
	SSeqid   string
	PIdent   float64
	Length   int
	Mismatch int
	Gapopen  int
	QStart   int
	QEnd     int
	SStart   int
	SEnd     int
	Evalue   float64
	Bitscore int
}

var ErrFieldCount = errors.New("wrong number of fields in line")

// A parser for the NCBI blast output format.
// Currently only support the table format with standard fields.
type TableReader struct {
	Comma   rune // Field delimiter (set to ',' by NewTableReader, which supports -outfmt 10)
	Comment rune // Comment character for start of line (default '#')

	r *csv.Reader
}

func NewTableReader(r io.Reader) *TableReader {
	reader := &TableReader{
		Comma:   ',',
		Comment: '#',
		r:       csv.NewReader(r),
	}

	reader.r.Comment = reader.Comment

	return reader
}

// Read reads one record from r.
// Each record is a TableEntry.
// It currently only works on standard output with table formation (-outfmt 6,7,10)
func (r *TableReader) Read() (entry TableEntry, err error) {
	record, err := r.r.Read()
	if err != nil {
		return
	}

	if len(record) != 12 {
		err = ErrFieldCount
		return
	}
	entry = TableEntry{}
	entry.QSeqid = record[0]
	entry.SSeqid = record[1]
	entry.PIdent, _ = strconv.ParseFloat(record[2], 64)
	entry.Length, _ = strconv.Atoi(record[3])
	entry.Mismatch, _ = strconv.Atoi(record[4])
	entry.Gapopen, _ = strconv.Atoi(record[5])
	entry.QStart, _ = strconv.Atoi(record[6])
	entry.QEnd, _ = strconv.Atoi(record[7])
	entry.SStart, _ = strconv.Atoi(record[8])
	entry.SEnd, _ = strconv.Atoi(record[9])
	entry.Evalue, _ = strconv.ParseFloat(record[10], 64)
	entry.Bitscore, _ = strconv.Atoi(record[3])

	return entry, nil

}

// ReadAll reads all the remaining records from r.
// Each record is a TableEntry. 
// It currently only works in standard output with table format (-outfmt 6,7,10).
// A successful call returns err == nil, not err == EOF.
func (r *TableReader) ReadAll() (entries []TableEntry, err error) {
	for {
		entry, err := r.Read()
		if err == io.EOF {
			return entries, nil
		}
		if err != nil {
			return nil, err
		}
		entries = append(entries, entry)
	}
	panic("unreachable")
}

// Blast searches a sequence query against subject sequences or a sequence database.
// Supported programs:
// blastn: searches nucleotide sequence against a nucleotide database.
// blastp: searches protein sequence against a protein database.
// blastx: translate a nucleotide query and searches it against a protein database.
// tblastn: searches a protein query against nucleotide subject sequences or a nucleotide database translated at search time.
// tblastx: searches a translated nucleotide query against translated nucleotide subject sequences or a translated nucleotide database. 
// rpsblast: searches a protein query against the conserved domain database (CDD), which is a set of protein profiles.
func Blast(program string, stdin io.Reader, stdout, stderr io.Writer, options ...string) error {
	return cmdline(program, stdin, stdout, stderr, options...)
}

// Makeblastdb builds a BLAST database.
// More information about makeblastdb, see http://www.ncbi.nlm.nih.gov/books/NBK1763/
// Basically, pass database you want to create to Stdin.
func Makeblastdb(stdin io.Reader, stdout, stderr io.Writer, options ...string) error {
	return cmdline("makeblastdb", stdin, stdout, stderr, options...)
}

// Segmasker: Low complexity region masker based on the SEG algorithm
func Segmasker(stdin io.Reader, stdout, stderr io.Writer, options ...string) error {
	return cmdline("segmasker", stdin, stdout, stderr, options...)
}

func cmdline(cmdname string, stdin io.Reader, stdout, stderr io.Writer, options ...string) (err error) {
	cmd := exec.Command(cmdname, options...)
	cmd.Stdin = stdin
	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err = cmd.Run()

	return
}
