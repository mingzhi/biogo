package biogo

// A FeatureLocation specifies the start, end and strand of a sequence feature.
// Start and End specify the value when the feature begins and ends.
// Strand uses 1 for the forward (plus) string, -1 for the reverse (negative) strain.
type FeatureLocation struct {
	Start  int
	End    int
	Strand int
}

// NewFeatureLocation returns a FeatureLocation.
func NewFeatureLocation(start, end, strand int) *FeatureLocation {
	return &FeatureLocation{
		Start:  start,
		End:    end,
		Strand: strand,
	}
}

// A SeqFeature represents a sequence feature as an object.
type SeqFeature struct {
	Location         *FeatureLocation // the loation of the feature on the sequence.
	Type             string           // the specified type of the feature (ie. CDS, exon, ...)
	LocationOperator string           // a string specifying how this SeqFeature may be related to others, such as "join".
	Id               string           // identifier for the feature
	Ref              string           // A reference to another equence.
	RefDb            string           // A different database for the reference accession number.
	SubFeatures      []SeqFeature     // Additional SeqFeature which fall under this "parent" feature.
}

// NewSeqFeature returns a SeqFeature object.
func NewSeqFeature(id string) *SeqFeature {
	return &SeqFeature{
		Id: id,
	}
}
