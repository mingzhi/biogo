package biogo

import (
	"bitbucket.org/mingzhi/biogo/alphabet"
)

type Sequence struct {
	Data     string
	Alphabet *alphabet.Alphabet
}

func NewSequence(data string, alphabet *alphabet.Alphabet) *Sequence {
	return &Sequence{Data: data, Alphabet: alphabet}
}

type SeqRecord struct {
	Id          string
	Desc        string
	Seq         *Sequence
	Annotations map[string]string
}

func NewSeqRecord(id, desc string, seq *Sequence) *SeqRecord {
	return &SeqRecord{Id: id,
		Desc: desc,
		Seq:  seq,
	}
}
